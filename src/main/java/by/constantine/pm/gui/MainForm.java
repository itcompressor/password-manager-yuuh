package by.constantine.pm.gui;

import by.constantine.pm.gui.component.TextAreaCellEditor;
import by.constantine.pm.gui.component.TextAreaCellRenderer;
import com.constantine.yuuh.utils.Setting;
import com.constantine.yuuh.Yuuh;
import com.constantine.yuuh.model.Account;
import com.constantine.yuuh.model.Data;
import by.constantine.pm.utils.HttpManager;
import com.constantine.yuuh.utils.LocalStorage;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class MainForm extends javax.swing.JFrame {

    private static final Logger logger = Logger.getLogger(MainForm.class.getName());

    private Data data;
    private Account selectAccount = null;

    private DefaultTableModel tableModel;
    private TableRowSorter<TableModel> sorter;

    private String passwordHide;

    private LocalStorage localStorage;
    private Alarm alarm;
    private Setting setting;
    private Yuuh yuuh;

    public MainForm() {
        initComponents();
        yuuh = Yuuh.getInstance();
        alarm = Alarm.getInstance();
        setting = Setting.getInstance();
        localStorage = LocalStorage.getInstance();
        passwordHide = setting.getPasswordHide();
        System.out.println("Working Directory = "
                + System.getProperty("user.dir"));
        data = yuuh.getData();
        setAndConfigureTable(table);
    }

    public void setAndConfigureTable(final JTable table) {
        tableModel = (DefaultTableModel) table.getModel();
        sorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.setRowHeight(50);
        ListSelectionModel cellSelectionModel = table.getSelectionModel();
        cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        table.setDefaultRenderer(String.class, new TextAreaCellRenderer());
        table.setDefaultEditor(String.class, new TextAreaCellEditor());
        table.getDefaultEditor(String.class).addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingCanceled(ChangeEvent e) {
                logger.info("The user canceled editing.");
            }

            @Override
            public void editingStopped(ChangeEvent e) {
                logger.info("The user stopped editing successfully.");
                int[] selectedRow = table.getSelectedRows();
                int[] selectedColumn = table.getSelectedColumns();
                for (int i = 0; i < selectedRow.length; i++) {
                    for (int j = 0; j < selectedColumn.length; j++) {
                        int row = selectedRow[i];
                        updateAccount(row);
                    }
                }
            }
        });
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    int[] selectedRow = table.getSelectedRows();
                    for (int i = 0; i < selectedRow.length; i++) {
                        int row = selectedRow[i];
                        int rowCount = table.getRowCount();
                        for (int j = 0; j < rowCount; j++) {
                            table.setValueAt(passwordHide, j, 4);
                        }
                        int id = (Integer) table.getValueAt(row, 0);
                        selectAccount = findAccountById(id);
                        table.setValueAt(selectAccount.getPassword(), row, 4);
                    }
                }
            }
        });
        for (int i = 0; i < data.getAccounts().size(); i++) {
            Account account = data.getAccounts().get(i);
            tableModel.addRow(account.toTableRow(passwordHide));
        }

    }

    public Account findAccountById(int id) {
        for (Account account : data.getAccounts()) {
            if (account.getId() == id) {
                return account;
            }
        }
        return null;
    }

    //TODO: refactor
    private void updateAccount(int row) {
        Account account = findAccountById((Integer) table.getValueAt(row, 0));
        account.setDescription((String) table.getValueAt(row, 1));
        account.setTarget((String) table.getValueAt(row, 2));
        account.setLogin((String) table.getValueAt(row, 3));
        account.setPassword((String) table.getValueAt(row, 4));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        search = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        menu = new javax.swing.JMenuBar();
        fileCategory = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        openItem = new javax.swing.JMenuItem();
        syncLocalItem = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Password Manager Yuuh");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closing(evt);
            }
        });

        table.setAutoCreateRowSorter(true);
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Description", "Target", "Login", "Password"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false,true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table.setDragEnabled(true);
        table.setInheritsPopupMenu(true);
        jScrollPane1.setViewportView(table);

        search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                search(evt);
            }
        });

        jLabel1.setText("Search :");

        fileCategory.setText("File");

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("New");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newFilePerformed(evt);
            }
        });
        fileCategory.add(jMenuItem4);

        openItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openItem.setText("Open");
        openItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openItemActionPerformed(evt);
            }
        });
        fileCategory.add(openItem);

        syncLocalItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        syncLocalItem.setText("Save to local storage");
        syncLocalItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                syncLocalItemActionPerformed(evt);
            }
        });
        fileCategory.add(syncLocalItem);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Sync repository");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SyncRepositoryPerformed(evt);
            }
        });
        fileCategory.add(jMenuItem1);

        menu.add(fileCategory);

        jMenu1.setText("Tools");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Options");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OptionsPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Add account");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        menu.add(jMenu1);

        jMenu2.setText("Help");

        jMenuItem3.setText("About");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AboutPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        menu.add(jMenu2);

        setJMenuBar(menu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(search)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closing
        List<Account> accountsForSave = new ArrayList();
        for (Account account : data.getAccounts()) {
            if (account.getLogin().equals("")
                    && account.getDescription().equals("")
                    && account.getTarget().equals("")
                    && account.getPassword().equals("")) {
            } else {
                accountsForSave.add(account);
            }
        }
        data.setAccounts(accountsForSave);
        yuuh.save(data);
    }//GEN-LAST:event_closing

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        int id = data.getAccounts().size() + 1;
        Account account = new Account(id);
        data.getAccounts().add(account);
        tableModel.addRow(account.toTableRow(passwordHide));
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void syncLocalItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_syncLocalItemActionPerformed
        alarm.info(localStorage.save(data));
    }//GEN-LAST:event_syncLocalItemActionPerformed

    private void newFilePerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newFilePerformed
        alarm.info("System", "Soon...");
    }//GEN-LAST:event_newFilePerformed

    private void openItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openItemActionPerformed
        alarm.info("System", "Soon...");
    }//GEN-LAST:event_openItemActionPerformed

    private void SyncRepositoryPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SyncRepositoryPerformed
        alarm.info("Server", HttpManager.excutePost("http://localhost:8080/passwd/answer"));
    }//GEN-LAST:event_SyncRepositoryPerformed

    private void OptionsPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OptionsPerformed
        alarm.info("System", "Soon...");
    }//GEN-LAST:event_OptionsPerformed

    private void AboutPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AboutPerformed
        alarm.info("System", "Soon...");
    }//GEN-LAST:event_AboutPerformed

    private void search(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search
        String filter = search.getText();
        if (filter.length() == 0) {
            sorter.setRowFilter(null);
        } else {
            sorter.setRowFilter(RowFilter.regexFilter(filter));
        }
        logger.info(filter);

    }//GEN-LAST:event_search

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu fileCategory;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuBar menu;
    private javax.swing.JMenuItem openItem;
    private javax.swing.JTextField search;
    private javax.swing.JMenuItem syncLocalItem;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
