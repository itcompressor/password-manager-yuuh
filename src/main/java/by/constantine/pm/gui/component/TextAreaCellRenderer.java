/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.constantine.pm.gui.component;

import java.awt.Component;
import java.util.Objects;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;

public class TextAreaCellRenderer implements TableCellRenderer {

    private final JTextArea textArea = new JTextArea();

    public TextAreaCellRenderer() {
        textArea.setLineWrap(true);
        textArea.setBorder(BorderFactory.createEmptyBorder(1, 5, 1, 5));
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {
        if (isSelected) {
            textArea.setForeground(table.getSelectionForeground());
            textArea.setBackground(table.getSelectionBackground());
        } else {
            textArea.setForeground(table.getForeground());
            textArea.setBackground(table.getBackground());
        }
        textArea.setFont(table.getFont());
        textArea.setText(Objects.toString(value, ""));
        return textArea;
    }
}
