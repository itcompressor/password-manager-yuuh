package by.constantine.pm.gui;

import com.constantine.yuuh.model.Info;
import javax.swing.JOptionPane;

public class Alarm {
    
    private static volatile Alarm instance;
    
    public static Alarm  getInstance() {
        if(instance == null) {
            synchronized(Alarm.class) {
                if(instance == null) {
                    instance = new Alarm();
                }
            }
        }
        return instance;
    }

    public void info(String title, String text) {
        JOptionPane.showMessageDialog(null, text, title, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void error(String title, String text) {
        JOptionPane.showMessageDialog(null, text, title, JOptionPane.ERROR_MESSAGE);
    }

    public void info(Info info) {
        int option;
        if (info.isSuccess()) {
            option = JOptionPane.INFORMATION_MESSAGE;
        } else {
            option = JOptionPane.ERROR_MESSAGE;
        }
        JOptionPane.showMessageDialog(null, info.getMessage(), info.getTitle(), option);
    }
}
