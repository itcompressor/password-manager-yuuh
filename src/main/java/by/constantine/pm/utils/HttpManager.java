package by.constantine.pm.utils;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;

public class HttpManager {
    
    public static String excutePost(String targetURL) {
        HttpURLConnection connection = null;
        try {
            URL siteURL = new URL(targetURL);
            connection = (HttpURLConnection) siteURL.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "");
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.getOutputStream();
            InputStream stream = connection.getInputStream();
            StringBuilder response;
            try (BufferedReader readStream = new BufferedReader(new InputStreamReader(stream))) {
                String line;
                response = new StringBuilder();
                while ((line = readStream.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
            }
            return response.toString();

        } catch (Exception e) {
            return null;

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static Image loadImage(String URL) throws IOException {
        BufferedImage image = ImageIO.read(new URL(URL));
        return image;
    }

}
