package by.constantine.pm.utils;

import com.constantine.yuuh.i.AES;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.util.Arrays;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESjdkOnly implements AES {

    private static final Logger logger = Logger.getLogger(AESjdkOnly.class.getName());
    private final String algorithm = "AES";
    private byte[] key = "!@#$!@#$%^&**&^%".getBytes();
    private static volatile AESjdkOnly instance;

    private AESjdkOnly() {
    }

    public static AESjdkOnly getInstance() {
        logger.info("get instance");
        if (instance == null) {
            synchronized (AESjdkOnly.class) {
                if (instance == null) {
                    instance = new AESjdkOnly();
                }
            }
        }
        return instance;
    }

    @SuppressWarnings("UseSpecificCatch")
    public String encrypt(String data) {
        byte[] dataToSend = data.getBytes();
        Cipher cipher;
        byte[] encryptedData = "".getBytes();
        try {
            cipher = Cipher.getInstance(algorithm);
            SecretKeySpec secretKey = new SecretKeySpec(key, algorithm);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            encryptedData = cipher.doFinal(dataToSend);
        } catch (Exception e) {
            logger.warning(e.toString());
        }
        return Base64.encode(encryptedData);//.toString();
    }

    @SuppressWarnings("UseSpecificCatch")
    public String decrypt(String data) {
        byte[] encryptedData = Base64.decode(data);
        byte[] decrypted = "".getBytes();
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(algorithm);
            SecretKeySpec secretKey = new SecretKeySpec(key, algorithm);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            decrypted = cipher.doFinal(encryptedData);
        } catch (Exception e) {
            logger.warning(e.toString());
        }
        return new String(decrypted);
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        if (key.length > 16) {
            this.key = Arrays.copyOfRange(key, 0, 16);
        } else {
            this.key = key;
        }
    }
}
