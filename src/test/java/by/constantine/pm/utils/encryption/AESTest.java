/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.constantine.pm.utils.encryption;

import by.constantine.pm.utils.AESjdkOnly;
import com.constantine.yuuh.utils.MD5;
import org.junit.Test;
import static org.junit.Assert.*;


public class AESTest {
    
    @Test
    public void testOriginEncyptDecrypt() {
        AESjdkOnly aes = AESjdkOnly.getInstance();
        String password = "123456789";
        String encryptedPassword = aes.encrypt(password);
        String dectyptedPassword = aes.decrypt(encryptedPassword);
        assertEquals(password, dectyptedPassword);
    }  
    
    @Test
    public void testNewestEncyptDecrypt() {
        AESjdkOnly aes = AESjdkOnly.getInstance();
        aes.setKey(MD5.getHash("loha1234").getBytes());
        String password = "123456789";
        String encryptedPassword = aes.encrypt(password);
        String dectyptedPassword = aes.decrypt(encryptedPassword);
        assertEquals(password, dectyptedPassword);
    }
    
}
